import unittest.mock as mock

import pytest


@pytest.fixture
def hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    with mock.patch("sys.argv", ["state", "thing"]):
        hub.pop.config.load(["idem", "output", "acct"], cli="idem", parse_cli=False)
    yield hub
