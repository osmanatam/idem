import subprocess
import sys

import pop.hub
import pytest

from idem.exec.init import ExecReturn


def run_ex(path, args, kwargs, acct_file=None, acct_key=None):
    """
    Pass in an sls list and run it!
    """
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="idem")
    hub.states.test.ACCT = ["test_acct"]
    hub.pop.loop.create()
    ret = hub.pop.Loop.run_until_complete(
        hub.idem.ex.run(path, args, kwargs, acct_file, acct_key)
    )
    assert isinstance(ret, ExecReturn)
    assert bool(ret) is ret.result
    assert ret.result is ret["result"]
    assert ret.comment is ret["comment"]
    assert ret.ref is ret["ref"]
    return ret


def test_shell_exec(runpy):
    cmd = [sys.executable, runpy, "exec", "test.ping", "--output=exec"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert ret.returncode == 0
    assert b"True" in ret.stdout


def test_shell_exec_fail(runpy):
    cmd = [sys.executable, runpy, "exec", "test.ping", "extra_arg", "--output=exec"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert ret.returncode == 1


def test_exec(code_dir, hub):
    acct_fn = code_dir.joinpath("tests", "files", "acct.fernet")
    acct_key = "eWO2UroAYY3Dff8uKcT32iiBHW2qVkVaDV3vIQoIaJU="
    ret = run_ex("test.ctx", [], {}, acct_file=acct_fn, acct_key=acct_key)
    assert ret == ExecReturn(
        result=True,
        comment=None,
        ret={"acct": {"foo": "bar"}},
        ref=hub.exec.test.ctx.func.__module__,
    )


def test_deprecation(hub):
    with pytest.warns(
        DeprecationWarning, match="Exec modules must return a dictionary"
    ):
        ret = hub.exec.test.echo("value")

    assert ret == "value"


async def test_adeprecation(hub):
    with pytest.warns(
        DeprecationWarning, match="Exec modules must return a dictionary"
    ):
        ret = await hub.exec.test.aecho("value")

    assert ret == "value"


def test_ping(hub):
    ret = hub.exec.test.ping()
    assert ret == ExecReturn(
        result=True, ref=hub.exec.test.ping.func.__module__, ret=True, comment=None
    )
    assert isinstance(ret, ExecReturn)
    assert bool(ret) is ret.result
    assert ret.result is ret["result"]
    assert ret.comment is ret["comment"]
    assert ret.ref is ret["ref"]
    return ret


async def test_aping(hub):
    ret = await hub.exec.test.aping()
    assert ret == ExecReturn(
        result=True, ref=hub.exec.test.aping.func.__module__, ret=True, comment=None
    )
    assert isinstance(ret, ExecReturn)
    assert bool(ret) is ret.result
    assert ret.result is ret["result"]
    assert ret.comment is ret["comment"]
    assert ret.ref is ret["ref"]
    return ret


def test_failure(hub):
    with pytest.raises(Exception, match="Expected failure"):
        run_ex("test.fail", [], {})


def test_asyncfailure(hub):
    with pytest.raises(Exception, match="Expected failure"):
        run_ex("test.afail", [], {})
