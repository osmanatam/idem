import uuid

import pytest


@pytest.mark.asyncio
async def test_empty(hub):
    # Test the results of a batch run with the minimum arguments and no states
    name = uuid.uuid4()
    task = hub.pop.Loop.create_task(hub.idem.state.batch(name=name, states={}))

    # Reading the states will fail
    with pytest.raises(ValueError):
        await task

    # Verify that everything was cleaned up
    ret = hub.idem.state.status(name=name)
    assert ret == {
        "acct_profile": "default",
        "errors": [],
        "running": {},
        "status": 0,
        "status_name": "FINISHED",
        "test": False,
    }


def test_undefined(hub):
    ret = hub.idem.state.status(name="undefined")
    assert ret == {
        "acct_profile": "",
        "errors": [],
        "running": {},
        "status": -4,
        "status_name": "UNDEFINED",
        "test": None,
    }


async def test_running(hub):
    name = uuid.uuid4()
    # Run states defined within a dictionary in code
    task = hub.pop.Loop.create_task(
        hub.idem.state.batch(
            name=name,
            states={"state name": {"test.succeed_without_changes": {"name": "name"}}},
        )
    )

    # Wait for the task to start
    await hub.pop.loop.sleep(1)

    # Get the current status of the state run
    ret = hub.idem.state.status(name=name)

    # Verify the return results of the running sttaes
    assert ret == {
        "acct_profile": "default",
        "errors": [],
        "running": {
            "test.succeed_without_changes_|-state name_|-state name_|-name": {
                "__run_num": 1,
                "changes": {},
                "comment": "The named state test.succeed_without_changes is not available",
                "name": "state " "name",
                "result": False,
            }
        },
        "status": 0,
        "status_name": "FINISHED",
        "test": False,
    }

    await task
